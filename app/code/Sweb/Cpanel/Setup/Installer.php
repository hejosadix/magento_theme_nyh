<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Sweb\Cpanel\Setup;

use Magento\Framework\Setup;

class Installer implements Setup\SampleData\InstallerInterface {

    /**
     * @var \Magento\CmsSampleData\Model\Page
     */
    private $page;

    /**
     * @var \Magento\CmsSampleData\Model\Block
     */
    private $block;

    /**
     * @param \Sweb\Cpanel\Model\Page $page
     * @param \Sweb\Cpanel\Model\Block $block
     */
    public function __construct(
    \Sweb\Cpanel\Model\Page $page, 
            \Sweb\Cpanel\Model\Block $block
    ) {
        $this->page = $page;
        $this->block = $block;
    }

    /**
     * {@inheritdoc}
     */
    public function install() {

        //$this->page->install(['Sweb_Cpanel::fixtures/pages/pages.csv']);
        $this->page->install(
                [

                    'Sweb_Cpanel::cmspages/cms_pages.csv',
                ]
        );
        $this->block->install(
                [

                    'Sweb_Cpanel::cmsblocks/cms_static_blocks.csv',
                ]
        );
    }

}
