<?php


namespace Sweb\Soldout\Model\Config\Source;

class StockOption implements \Magento\Framework\Option\ArrayInterface 
{
    /**
     * @return array
     */
    public function toOptionArray() {
        return [
            ['value' => 'label', 'label' => __('Label')],
            ['value' => 'image', 'label' => __('Image')]
        ];
    }

}
?>

