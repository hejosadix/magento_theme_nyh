<?php

namespace Sweb\Soldout\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper 
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    
    public function __construct(
        \Magento\Framework\App\Helper\Context $context, 
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    public function getMediaUrl() {
        return $this->_storeManager
                ->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    public function getConfigValue($value = '') {
        return $this->scopeConfig
                ->getValue($value, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
        
}
